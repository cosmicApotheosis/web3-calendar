# Web 3.0 Personal Calendar
## ConsenSys Academy 2018 Developer Program Final Project

This is a decentralized application that allows users to create their own calendar instance on an ethereum blockchain, and then add, update, and delete events on that calendar. Only the user that creates a calendar instance may make any changes to that calendar. The front-end portion of the application is hosted on IPFS.

## Calendar Usage

Upon visiting the landing page, the user is greeted with a prompt to create a new calendar. Once the Metamask transaction is submitted and then mined, the user is then redirected to their newly created calendar. 

On the calendar page are options to create, update, and delete events on the calendar. 

To create a new event, the user must input a title, start date-time, end date-time, and description. After submitting the Metamask transaction to add the event, the calendar will update once the transaction is mined and the newly added event will appear with an id enclosed in brackets before the title, eg: [1000] My first event!

This id that precedes the event title is then used in the updating and deleting of events. To update or delete an event, the user must enter the id of the event that is to be updated/deleted.

If the user wishes to delete their calendar instance altogether, below the calendar is a button that allows them to do so. After the transaction is mined, they will be redirected back to the landing page where they are once again greeted with the option to create a new calendar. 

## Try The Rinkeby Demo app

1. Download and install Metamask extension for your browser: https://metamask.io/

2. Get some Rinkeby ether from: https://faucet.rinkeby.io/ (you can create a nonsense post using Google+ if you lack Twitter etc.)

3. The dapp is hosted on IPFS. It may be accessed via Qmf3YcHP3Vn4aENzaCAg5HSRJJN4hSBEvVYGLhhmE4tf3Q, and if you don't run your own node, you can use https://ipfs.io/ipfs/Qmf3YcHP3Vn4aENzaCAg5HSRJJN4hSBEvVYGLhhmE4tf3Q. If you see a blank screen that says "loading dapp" make sure you have Metamask installed, are logged in, and connected to the Rinkeby network.

## Run app locally

### Tested versions

This application was developed using Ubuntu 16.04 and has only been tested using that OS. It is likely that the application will not work correctly in Windows, especially due to the use of several symlinks. 

1. Node v9.11.2
2. Ganache CLI v6.1.6 (ganache-core: 2.1.5)
3. Truffle v4.1.12 (core: 4.1.12) with Solidity v0.4.24 (solc-js)

### Instructions

1. Install Truffle and Ganache CLI globally. (or the GUI version of Ganache, alternatively)
    ```sh
    npm install -g truffle@4.1.12
    npm install -g ganache-cli@6.1.6
    ```

2. Clone this repository.
    ```sh
    git clone https://gitlab.com/cosmicApotheosis/web3-calendar.git
    cd web3-calendar/front-end
    ```

3. Install npm packages.
    ```sh
    npm install
    ```

4. Run the development blockchain.
    ```sh
    cd ..
    ganache-cli
    ```

5. Compile and migrate the smart contracts.
    ```sh
    truffle compile
    truffle migrate
    ```

6. Run the dev server for front-end hot reloading (outside the development console). Smart contract changes must be manually recompiled and migrated.
    ```sh
    cd front-end
    // Serves the front-end on http://localhost:3000
    npm run start
    ```

7. In Metamask, ensure the network specified is localhost:8545 (or matches the dev blockchain)

### Testing

1. Inside the project root directory, simply enter.
    ```sh
    truffle test
    ```

2. Explanations of each test are documented in the comments in the test file. A high level overview of the tests is given in the document ./design_pattern_decisions.md.


