# Avoiding Common Attacks

## Logic Bugs

Rigorous tests ensure that calendars and calendar events can be added, updated, and deleted only by the appropriately permissioned users.

Require statements are used extensively in conjunction with relatively simple code to minimize the risk of bugs. 

## Failed Sends

As this project exists on a testnet where the ether has no actual value, the gas included with transactions is intentionally set to be high to ensure transactions go through. 

## Recursive Calls

This is not a particular risk for this project, as transfers of value are not part of the project. 

## Integer Arithmetic Overflow

The only case where this could potentially happen is if a user's list of calendar events grows too large (since the ID assigned to each event is a uint). This seems like an unlikely an trivial possiblity. 

## Poison Data 

Only an individual user's contract accepts string and date inputs, thus those inputs are not exposed to any contract other than their own. The factory contract that contains addresses of everybody's contracts accepts no such inputs.

## Exposed Functions

Every function has an access modifier to ensure only the rightful owner may make changes to contract state.

## Exposed Secrets

All code and data on the blockchain is visible by anyone, even if not marked as "public" in Solidity. Contracts that attempt to rely on keeping keys or behaviour secret are in for a surprise.The contracts in this project do not rely on any secret information.

## Denial of Service / Dust Spam

The input for a given contract is limited in the browser, and if the right conditions aren't met, the transaction reverts.

## Miner Vulnerabilities

Nothing in this project is sensitive to the timing of new blocks added to the blockchain.

## Malicious Creator

The project uses OZ ownership functions to ensure that only the rightful owner to a contract may behave in the ways specified. These contracts dont use transfers of value to exploit.

## Off-chain Safety

Everything is stored on chain, and as such there is no sensitive information being transferred.

## Cross-chain Replay Attacks

This is a testnet project.

## Tx.Origin Problem

The modifier isOwner requires the caller to b explicitly stated as a parameter, and tx.origin is not used.

## Solidity Function Signatures and Fallback Data Collisions

No fallback function is used in this project, as users should not be sending ether.

## Incorrect use of Cryptography

Cryptographic functions are not used in this project.

## Gas Limits

The Solidity CRUD pattern implemented to manage Calendar and Events is designed to minimize the risk of excessive gas usage with large lists.

## Stack Depth Exhaustion

Similar to the failed sends section, the appropriate require statements are included in functions to reduce the consequences of such occurances. 