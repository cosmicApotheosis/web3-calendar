pragma solidity ^0.4.24;
// Import via npm instead of ethPM
// Destructible is Ownable, and the factory contract that creates this contract is the owner. 
// Additional permissions are required in the factory contract to ensure that only the 
// appropriate caller may destroy their contract.
// a symlink is necessary for truffle compile to see that this library is installed
import 'openzeppelin-solidity/contracts/lifecycle/Destructible.sol';

// The Solidity CRUD pattern as outlined here:
// https://medium.com/@robhitchens/solidity-crud-part-2-ed8d8b4f74ec
// Is used to manage a user's calendar events.
// There is a bug with this system though where a user can't delete the first item stored in the list.
// As a quick fix, a filler item is pushed to the list array in the contract constructor,
// and the front-end of the application just ignores that first list item. Not sure what the best way to 
// fix this is.

contract Calendar is Destructible {
  // this is the address of the user that instantiates the calendar contract
  address owner;
  // address of the contract that will instantiate this contract
  address factory;
  // this counter ensures each calendar event is assigned a unique id
  uint idCounter = 1000;

  // this event format matches the format of the React frontend, plus a index pointer
  struct CalendarEvent {
    string title;
    uint start;
    uint end;
    string desc;
    uint index;
  }

  // this maps the id of each calendar, uint is just an id number
  mapping (uint => CalendarEvent) calendarEvents;

  // ids of the calendars
  uint[] calendarEventsList;

  // event for new calendar events
  event NewCalendarEventAdded (uint id);
  event CalendarEventUpdated (uint id);
  event CalendarEventDeleted (uint id);

  // modifier to ensure calendar functions only accessible to owner of calendar
  modifier isOwner(address _caller) {
        require(msg.sender == factory);
        require(_caller == owner);
        _;
    }

  // Constructor
  constructor (address _owner) public {
    owner = _owner;
    factory = msg.sender;
    // this is done since you can't "delete" the zero index item in the mapping/array
    calendarEventsList.push(999);
  }

  // checks if a calendar event exists with a given id
  function eventExists (uint calendarEventId) private constant returns (bool doesIndeed) {
    if (calendarEventsList.length == 0) return false;
    return (calendarEventsList[calendarEvents[calendarEventId].index] == calendarEventId);
  }

  // Returns the count of calendars stored in calendarsList array
  function getCalendarEventsCount(address caller) public isOwner(caller) constant returns (uint) {
    return calendarEventsList.length;
  }
  // CRUD
  function createNewCalendarEvent (address caller, string title, uint start, uint end, string desc) public isOwner(caller) returns (bool success) {
    // a unique id is assigned to each new calendar event. we dont need to check that an event with this id already exists since
    // the id has nothing to do with the properties of the new calendar event
    //if (eventExists(id)) revert();
    CalendarEvent memory newCalendarEvent;

    newCalendarEvent.title = title;
    //newCalendarEvent.allDay = allDay;
    newCalendarEvent.start = start;
    newCalendarEvent.end = end;
    newCalendarEvent.desc = desc;
    // 2 in 1 move here
    newCalendarEvent.index = calendarEventsList.push(idCounter) - 1;

    calendarEvents[idCounter] = newCalendarEvent;

    emit NewCalendarEventAdded (idCounter);
    idCounter++;
    return true;
  }
  // Is this one even necessary?? YES
  function getCalendarEvent (address caller, uint idToGet) public isOwner(caller) constant returns (uint id, string title, uint start, uint end, string desc, uint index) {
    //make sure its actually an event
    if (!eventExists(idToGet)) revert();

    return (
      idToGet,
      calendarEvents[idToGet].title,
      //calendarEvents[idToGet].allDay,
      calendarEvents[idToGet].start,
      calendarEvents[idToGet].end,
      calendarEvents[idToGet].desc,
      calendarEvents[idToGet].index);
  }

  function getAllCalendarEvents (address caller) public isOwner(caller) constant returns (uint[]) {
    return calendarEventsList;
  }

  function updateCalendarEvent (address caller, uint idToUpdate, string title, uint start, uint end, string desc) public isOwner(caller) returns (bool success) {
    //make sure its actually an event
    if (!eventExists(idToUpdate)) revert();

    CalendarEvent memory updatedCalendarEvent;

    updatedCalendarEvent.title = title;
    //updatedCalendarEvent.allDay = allDay;
    updatedCalendarEvent.start = start;
    updatedCalendarEvent.end = end;
    updatedCalendarEvent.desc = desc;
    updatedCalendarEvent.index = calendarEvents[idToUpdate].index;

    calendarEvents[idToUpdate] = updatedCalendarEvent;

    emit CalendarEventUpdated (idToUpdate);
    return true;
  }

  function deleteCalendarEvent (address caller, uint idToDelete) public isOwner(caller) returns (bool success) {
    // make sure thers actually a calendar event to delete
    if (!eventExists(idToDelete)) revert();

    uint rowToDelete = calendarEvents[idToDelete].index; // 0
    uint keyToMove = calendarEventsList[calendarEventsList.length - 1]; // 1000

    calendarEventsList[rowToDelete] = keyToMove;
    calendarEvents[keyToMove].index = rowToDelete;
    calendarEventsList.length--;

    emit CalendarEventDeleted (idToDelete);
    return true;
  }

}
