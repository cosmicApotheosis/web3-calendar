pragma solidity ^0.4.24;
// Import via npm instead of ethPM
// Allows for emergency stop operation of the dapp
// The UI doesn't have any way engage the emergency stop, just do it from remix
// the emergency stop is unable to prevent a user from interacting with a calendar they have already created,
// it just prevents the creation and deletion of calendars
// a symlink is necessary for truffle compile to see that this library is installed
import 'openzeppelin-solidity/contracts/lifecycle/Pausable.sol';

import './Calendar.sol';
// THIS CONTRACT IS ABLE TO CALL FUNCTIONS ON ITS CHILD CONTRACTS
contract CalendarFactory is Pausable {
  // a struct containing a calendar contract plus pointer to keep track of it in array
  struct CalendarStruct {
    Calendar cal;
    uint index;
  }
  //calendarFactoryInstance.calendars[this.account].cal.NewCalendarEventAdded
  // this maps the addresses of the OWNERS of each calendar
  mapping (address => CalendarStruct) public calendars;
  // addresses of the owners of the calendars
  address[] public calendarsList;

  // event for new calendar added
  event NewCalendarAdded (address owner);
  event CalendarDeleted (address owner);

  // checks if a user with a given address already has a calendar
  function hasCalendar (address calendarOwnerAddress) public constant returns (bool isIndeed) {
    if (calendarsList.length == 0) return false;
    return (calendarsList[calendars[calendarOwnerAddress].index] == calendarOwnerAddress);
  }

  // Returns the count of calendars stored in calendarsList array
  function getCalendarCount() public constant returns (uint) {
    return calendarsList.length;
  }

  // Returns the address of the calendar stored in the mapping
  function getCalendarAddress(address calendarOwnerAddress) public constant returns (address) {
    if (!hasCalendar(calendarOwnerAddress)) revert();
    return calendars[calendarOwnerAddress].cal;
  }

  function createNewCalendar (address calendarOwnerAddress) public whenNotPaused returns (bool success) {
    // user can only have 1 calendar per address
    if (hasCalendar(calendarOwnerAddress)) revert();
    calendars[calendarOwnerAddress].cal = new Calendar(calendarOwnerAddress);
    calendars[calendarOwnerAddress].index = calendarsList.push(calendarOwnerAddress) - 1;
    emit NewCalendarAdded (calendarOwnerAddress);
    return true;
  }

  function deleteCalendar (address calendarOwnerAddress) public whenNotPaused returns (bool success) {
    // make sure thers actually a calendar to delete
    if (!hasCalendar(calendarOwnerAddress)) revert();
    // ONLY THE OWNER OF A CALENDAR CAN DELETE IT!!!
    if (calendarOwnerAddress != msg.sender) revert();

    uint rowToDelete = calendars[calendarOwnerAddress].index;
    address keyToMove = calendarsList[calendarsList.length - 1];

    // call OZ destroy function here
    Calendar(calendars[msg.sender].cal).destroy();

    // remove destroyed function from list
    calendarsList[rowToDelete] = keyToMove;
    calendars[keyToMove].index = rowToDelete;
    calendarsList.length--;

    emit CalendarDeleted (calendarOwnerAddress);
    return true;
  }
  /*
  ** FUNCTIONS BELOW USED TO INTERACT WITH CHILD CONTRACT
  */

  function getCalendarEventsCount() public constant returns (uint) {
    // make sure user actually has a calendar
    if (!hasCalendar(msg.sender)) revert();
    return Calendar(calendars[msg.sender].cal).getCalendarEventsCount(msg.sender);
  }

  function createNewCalendarEvent (string title, uint start, uint end, string desc) public returns (bool success) {
    // make sure user actually has a calendar
    if (!hasCalendar(msg.sender)) revert();
    return Calendar(calendars[msg.sender].cal).createNewCalendarEvent(msg.sender, title, start, end, desc);
  }

  function getCalendarEvent (uint idToGet) public constant returns (uint id, string title, uint start, uint end, string desc, uint index) {
    // make sure user actually has a calendar
    if (!hasCalendar(msg.sender)) revert();
    return Calendar(calendars[msg.sender].cal).getCalendarEvent(msg.sender, idToGet);
  }

  function getAllCalendarEvents () public constant returns (uint[]) {
    // make sure user actually has a calendar
    if (!hasCalendar(msg.sender)) revert();
    return Calendar(calendars[msg.sender].cal).getAllCalendarEvents(msg.sender);
  }

  function updateCalendarEvent (uint idToUpdate, string title, uint start, uint end, string desc) public returns (bool success) {
    // make sure user actually has a calendar
    if (!hasCalendar(msg.sender)) revert();
    return Calendar(calendars[msg.sender].cal).updateCalendarEvent(msg.sender, idToUpdate, title, start, end, desc);
  }

  function deleteCalendarEvent (uint idToDelete) public returns (bool success) {
    // make sure user actually has a calendar
    if (!hasCalendar(msg.sender)) revert();
    return Calendar(calendars[msg.sender].cal).deleteCalendarEvent(msg.sender, idToDelete);
  }
}
