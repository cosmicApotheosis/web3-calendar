# Design Pattern Decisions

## Introduction

At a high level, the idea was to create a decentralized application utilizing a blockchain that didn't involve the well established use case of transferring value. IPFS is used to host the front-end as opposed to a server via HTTP, while an ethereum blockchain takes the place of a database since IPFS can only host static data. The two technologies work together to create a fully peer to peer experience. 

## Smart Contract Design Patterns Utilized

This project very heavily leverages the Solidity CRUD programming pattern as documented by Rob Hitchens here: https://medium.com/@robhitchens/solidity-crud-part-2-ed8d8b4f74ec . Both the factory contract and the child calendar contracts use this same pattern which allows for Data Storage With Sequential Access, Random Access, and Delete. 

The calendar factory contract stores every user's calendar by storing a mapping of user addresses to calendar instances (technically, actually structs each containing a calendar instance and an index). To complement this, an array holds the user addresses of each active calendar instance. The order of addresses in the array is such that each element matches the index within the struct mapped to that same address in the mapping. When a calendar is deleted, it's user address is moved to the end of the array, the array is decremented, and the calendar's index that took the deleted calendar's place is updated within its respective struct. This is necessary since it is really the array that is keeping track of calendars, as the mapping still contains deleted calendar data. 

The calendar contract works in much the same way, only instead the mapping contains calendar event structs mapped to uint event Ids. The Id is simply a unique identifier that is used to retrieve, update, and delete events. Like within the factory contract, each struct contains an index which correspondes to the mapped Id's location within the array.

To make integration with the UI simplier, a user interacts with their calendar instance via the factory contract. This requires function modifiers which ensure that a user may only make changes to their own contract instance. The modifier isOwner ensures that 1) the msg.sender is the factory contract and 2) that the caller, as specified in the modifier parameter, is indeed the owner of the calendar. Within each of these functions is called within the factory contract, the parameter is specified as msg.sender (which is the user).

The project also leverages the OpenZeppelin Ownable contract in conjunction with the Destructible contract. Each user has the option of deleting their calendar instance. The OZ Destructible contract ensures that only the owner of the calendar may delete it - Calendar inherits from Destructible, and Destructible inherits from Ownable. The deletion of a calendar is performed via the factory contract, where the calendar is also removed from the list (the mapping and array). A user can then create a new calendar if they wish.

An emergency stop is implemented on the factory contract's create and delete calendar functions via OpenZeppelin's Pausable contract. The UI doesn't have any way for the admin to pause or unpause the contract, and remix must be used. The emergency stop isn't implemented on any of the calendar functions.

## Testing Overview

Access control and permissioning are critical parts of the application functionality, and the mocha test cases have been chosen to reflect this. The most important things to test are that a user may only make changes to and create/delete their own calendar instances. The OpenZeppelin assertRevert helper function is used to ensure function calls that should revert do so, and a similar helper function was created to ensure an error is received when a nonexistent contract function is called.  

## UI Design

The project had initially used Drizzle in the front-end, but eventually Drizzle was removed since it didn't update the UI reliably and led to a complicated and cryptic code base. Instead, key functions emit events, and event watchers are instantiated in each UI component's componentDidMount lifecycle method. The event watchers have a callback function specified that calls the relevent getter function needed to update the UI. Since these getters update redux state which is passed to the component as props, the UI re renders with each specified blockchain update. 

The application uses the web3.eth.getAccounts function to identify the currently logged in user and redirect them to the appropriate page. This is more a convenience to the user than it is a form of authentication, as it is easy enough to spoof the account address and and view someone else's calendar. A user's calendar event data is stored as contract state and is publicly accessible using etherscan and remix if not through the UI. 

## Potential Future Improvements

### uPort 

One improvement would be to integrate uPort into the application for both user authentication and transaction signing. A challenge here would revolve around the inability to watch for events using the uPort web3 provider.

### Off-chain event data storage

Currently, the list of user calendars as well as each users list of calendar events are all stored on chain. One improvement to improve scalability would be to store this data as JSON (or some other format) on IPFS and to store the hash of the data within the contracts. 

In this case, when a user would make a change to their calendar, the UI would read the current hash from the blockchain, use that hash to retreive the data from IPFS, make a change to that data in the browser, send that new data to IPFS, receive the new hash, then update the contract state hash variable with the new value by signing a Metamask transaction. 

## Scheduled Value Transfer

The ability to transfer a specified amount of ether to a specified user may be incorporated. 

## Known Bugs

The CalendarFactory contract's list management system appears to work ok. The management of events within a user's Calendar does have a bug.

The bug is that the first event within the list (meaning the mapping and array) can't be deleted. As a quick workaround, the contract constructor adds a filler event to the list, so that if a user wishes to delete the first event that they have added they will still be able to. The UI simply ignores this dummy event so from the perspective of the user they start with zero events on their calendar. 