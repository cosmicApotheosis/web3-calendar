import getWeb3 from '../utils/getWeb3';

import { WEB3_INITIALIZED, ACCOUNTS_INITIALIZED, CONTRACTS_INITIALIZED, CALENDAR_CREATED, GET_CALENDAR_EVENTS_COUNT, GET_CALENDAR_EVENTS } from './types';

export function initalizeDappState(contracts) {
  return dispatch => {
    getWeb3.then(result => {
      // send action to save web3 instance to store
      console.log('web3 instance sent to store: ', result);
      dispatch(web3Initialized(result));

      //use web3 instance to get accounts
      result.web3.eth.getAccounts((err, accounts) => {
        if (err) throw err;
        // send action to save accounts to store
        console.log('accounts sent to store: ', accounts);
        dispatch(accountsInitialized(accounts));
      });
      
      // convert contract JSON to truffle contract instance
      const truffleContract = require('truffle-contract');
      const truffleContractInstance = truffleContract(contracts);
      truffleContractInstance.setProvider(result.web3.currentProvider);
      truffleContractInstance.deployed().then(contract => {
        console.log('truffle contract sent to store: ', contract);
        dispatch(contractsInitialized(contract));
      });
    })
  };
}

export function web3Initialized(results) {
  return {
    type: WEB3_INITIALIZED,
    payload: results.web3
  };
}

export function accountsInitialized(accounts) {
  return {
    type: ACCOUNTS_INITIALIZED,
    payload: accounts
  };
}

export function contractsInitialized(contracts) {
  return {
    type: CONTRACTS_INITIALIZED,
    payload: contracts
  };
}

export function calendarCreated() {
  return {
    type: CALENDAR_CREATED,
    payload: true
  };
}

export function updateCalendarEventsCount(count) {
  return {
    type: GET_CALENDAR_EVENTS_COUNT,
    payload: count
  };
}

export function updateCalendarEvents(calendarEvents) {
  return {
    type: GET_CALENDAR_EVENTS,
    payload: calendarEvents
  };
}

// export function hasCalendarInitialized(hasCalendar) {
//   return {
//     type: HASCALENDARINITIALIZED,
//     payload: hasCalendar
//   };
// }

