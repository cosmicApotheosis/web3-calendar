import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import hasCalendarCheck from './hasCalendarCheck';
import { calendarCreated } from '../actions';

import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing.unit * 2,
  },
  appBar: {
    position: 'relative',
  },
  icon: {
    marginRight: theme.spacing.unit * 2,
  },
  heroUnit: {
    backgroundColor: theme.palette.background.paper,
  },
  heroContent: {
    maxWidth: 600,
    margin: '0 auto',
    padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
  },
  heroButtons: {
    marginTop: theme.spacing.unit * 4,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit * 6,
  },
  button: {
    margin: theme.spacing.unit,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
});

class CalendarCreate extends Component {

  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.contractEvent = this.contractEvent.bind(this);
  }

  async componentWillMount() {
    try {
      // local web3 variables
      this.web3 = this.props.web3;
      this.calendarFactoryInstance = this.props.contracts[0];
      this.account = this.props.accounts[0];
      // set up events
      this.calendarFactoryInstance.NewCalendarAdded(this.contractEvent);
    } catch (error) {
      console.log(error);
    }
  }

  async contractEvent(err, value) {
    // Whenver an event is emitted, a new calendar sucessfully has been created, update redux state
    console.log(JSON.stringify(value, null, 2));
    this.props.calendarCreated();
  }

  async createNewCalendar() {
    await this.calendarFactoryInstance.createNewCalendar(this.account, { from: this.account });
  }

  async handleClick(event) {
    event.preventDefault();
    await this.createNewCalendar();
  }

  render() {
    const { classes } = this.props;
    return(
      <React.Fragment>
      <CssBaseline />
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <CalendarTodayIcon className={classes.icon} />
          <Typography variant="title" color="inherit" noWrap>
            Web 3.0 Personal Calendar
          </Typography>
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        <div className={classes.heroUnit}>
          <div className={classes.heroContent}>
            <Typography variant="display1" align="center" color="textPrimary" gutterBottom>
              Hey {this.account}..
            </Typography>
            <Typography variant="title" align="center" color="textSecondary" paragraph>
              It looks like you don't yet have a calendar. Use the button below to create one!
            </Typography>
            <div className={classes.heroButtons}>
              <Grid container spacing={16} justify="center">
                <Grid item>
                  <Button onClick={this.handleClick} variant="contained" color="primary">
                    Create New Calendar
                  </Button>
                </Grid>
              </Grid>
            </div>
          </div>
        </div>
      </main>
    </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    accounts: state.accounts,
    //calendarEvents: state.calendarEvents,
    contracts: state.contracts,
    hasCalendar: state.hasCalendar,
    web3: state.web3
  };
}

const connectedCalendarCreate =  connect(mapStateToProps, { calendarCreated })(hasCalendarCheck(CalendarCreate));
export default withStyles(styles)(connectedCalendarCreate);

