import React, { Component } from 'react';
import { connect } from 'react-redux';

import moment from "moment";
import Calendar from "react-big-calendar";

import "react-big-calendar/lib/css/react-big-calendar.css";

import { updateCalendarEventsCount, updateCalendarEvents } from '../actions';
import CalendarContract from '../contracts/Calendar.json';

import NewEventForm from './NewEventForm';
import UpdateEventForm from './UpdateEventForm';
import DeleteEventForm from './DeleteEventForm';

import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing.unit * 2,
  },
  appBar: {
    position: 'relative',
  },
  icon: {
    marginRight: theme.spacing.unit * 2,
  },
  heroUnit: {
    backgroundColor: theme.palette.background.paper,
  },
  heroContent: {
    maxWidth: 600,
    margin: '0 auto',
    padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
  },
  heroButtons: {
    marginTop: theme.spacing.unit * 4,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit * 6,
  },
  button: {
    margin: theme.spacing.unit,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
});

Calendar.setLocalizer(Calendar.momentLocalizer(moment));

class CalendarShow extends Component {

  constructor(props) {
    super(props);

    this.contractEvent = this.contractEvent.bind(this);
    this.handleClickDelete = this.handleClickDelete.bind(this);
  }

  async componentDidMount() { 
    try {
      // local web3 variables
      this.web3 = this.props.web3;
      this.calendarFactoryInstance = this.props.contracts[0];
      this.account = this.props.accounts[0];
      // make sure you're on the right page
      this.shouldNavigateAway(); 

      // define Calendar truffle contract based on address in struct in CalendarFactory
      const calendarStruct = await this.calendarFactoryInstance.calendars(this.account);
      const calendarAddress = calendarStruct[0];
      const truffleContract = require('truffle-contract');
      const calendarTruffleContract = truffleContract(CalendarContract);
      calendarTruffleContract.setProvider(this.web3.currentProvider);
      // define calendar instance to be used within component
      this.calendarInstance = calendarTruffleContract.at(calendarAddress);

      // set up events
      this.calendarInstance.NewCalendarEventAdded(this.contractEvent);
      this.calendarInstance.CalendarEventUpdated(this.contractEvent);
      this.calendarInstance.CalendarEventDeleted(this.contractEvent);
      
      // on load, get calendar events count and events
      await this.getCalendarEventsCount();
      await this.getCalendarEvents();
    } catch (error) {
      console.log(error);
    }
  }

  async contractEvent(err, value) {
    // Whenver an event is emitted, update redux state
    console.log(JSON.stringify(value, null, 2));
    await this.getCalendarEventsCount();
    await this.getCalendarEvents();
  }

  async shouldNavigateAway() {
    // match.params prop is provided via react-router-dom
    if (this.props.match.params.account !== this.account) {
      console.log('I NEED TO LEAVE!!!!');
      this.props.history.push(`/`);
    }
  }

  async getCalendarEventsCount() {
    let eventsCount = await this.calendarFactoryInstance.getCalendarEventsCount.call({ from: this.account });
    this.props.updateCalendarEventsCount(eventsCount.toNumber());
  }

  async getCalendarEvents() {
    // these are just the ids of the events, need to iterate thru array to get event details for each
    let calendarEventIds = await this.calendarFactoryInstance.getAllCalendarEvents.call({ from: this.account });
    // console.log('calendarEventIds: ', calendarEventIds);
    // use the ids to retrieve an array of events where each event is an array
    let calendarEventArrays = [];

    // start at 1 since the contract automatically inserts a filler event at index 0
    for (var i = 1; i < calendarEventIds.length; i++) {
      // console.log('calendarEventIds[i]: ', calendarEventIds[i]);
      let event = await this.calendarFactoryInstance.getCalendarEvent.call(calendarEventIds[i], { from: this.account });
      calendarEventArrays.push(event);
    }

    // console.log('calendarEventArrays: ', calendarEventArrays);

    // convert the events from arrays into objects
    let calendarEvents = calendarEventArrays.map(event => {
      return(
        {
          id: event[0].toNumber(),
          title: `[${event[0].toNumber()}] ${event[1]}`,
          start: new Date(event[2].toNumber() * 1000),
          end: new Date(event[3].toNumber() * 1000),
          desc: event[4]
        }
      );
    });

    // console.log('calendarEvents: ', calendarEvents);

    // send events to redux store
    this.props.updateCalendarEvents(calendarEvents);
  }

  async handleClickDelete(event) {
    event.preventDefault();
    await this.calendarFactoryInstance.deleteCalendar(this.account, { from: this.account });
    this.props.history.push(`/`);
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
      <CssBaseline />
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <CalendarTodayIcon className={classes.icon} />
          <Typography variant="title" color="inherit" noWrap>
            Web 3.0 Personal Calendar
          </Typography>
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        <div className={classes.heroUnit}>
          <div className={classes.heroContent}>
            <Typography variant="display1" align="center" color="textPrimary" gutterBottom>
              Welcome {this.account}!
            </Typography>
            <Typography variant="title" align="center" color="textSecondary" paragraph>
              Use the buttons below to interact with your very own calendar on the (well, an..) Ethereum blockchain.. 
            </Typography>
            <div className={classes.heroButtons}>
              <Grid container spacing={16} justify="center">
                <Grid item>
                  {/* <Button variant="contained" color="primary">
                    Create New Event
                  </Button>*/}
                  <NewEventForm /> 
                </Grid>
                <Grid item>
                  {/* <Button variant="outlined" color="primary">
                    Update Existing Event
                  </Button>*/}
                  <UpdateEventForm /> 
                </Grid>
                <Grid item>
                  {/* <Button variant="outlined" color="secondary">
                    Delete Event
                  </Button>*/}
                  <DeleteEventForm /> 
                </Grid>
              </Grid>
            </div>
          </div>
        </div>
        <div className={classNames(classes.layout, classes.cardGrid)}>
          {/* End hero unit */}
          <Calendar
            defaultDate={new Date()}
            defaultView="month"
            events={ this.props.calendarEvents } 
            style={{ height: "100vh" }}
          />
        </div>
      </main>
      {/* Footer */}
      <footer className={classes.footer}>
        <Typography variant="title" align="center" gutterBottom>
          Delete Calendar
        </Typography>
        <Typography variant="subheading" align="center" color="textSecondary" component="p">
          The following button will destroy your smart contract instance:
          <br />
          <br />
          <Button variant="outlined" color="secondary" onClick={this.handleClickDelete}>
            Delete Calendar
          </Button>
        </Typography>
      </footer>
      {/* End footer */}
    </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    accounts: state.accounts,
    calendarEvents: state.calendarEvents,
    calendarEventsCount: state.calendarEventsCount,
    contracts: state.contracts,
    web3: state.web3
  };
}
const connectedCalendarShow = connect(mapStateToProps, { updateCalendarEventsCount, updateCalendarEvents })(CalendarShow);
export default withStyles(styles)(connectedCalendarShow);

