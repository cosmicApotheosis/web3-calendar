import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});


class DeleteEventForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };

    // modal handlers
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    // form is uncontrolled component
    this.handleSubmit = this.handleSubmit.bind(this);
    // create references
    this.inputId = React.createRef();
  }

  async componentDidMount() {
    try {
      // local web3 variables
      this.web3 = this.props.web3;
      this.calendarFactoryInstance = this.props.contracts[0];
      this.account = this.props.accounts[0];
    } catch (error) {
      console.log(error);
    }
  }

  async deleteCalendarEvent(id) {
    const deletedEvent = await this.calendarFactoryInstance.deleteCalendarEvent(id, { from: this.account });
    console.log(deletedEvent);
    return deletedEvent;
  }

  async handleSubmit(event) {
    event.preventDefault();

    const id = this.inputId.current.value;

    console.log(`submitted id: ${id}`);

    await this.deleteCalendarEvent(id);

    // reset form values
    this.inputId.current.value = null;

    // close modal
    this.handleClose();
  }

  handleClose() {
    this.setState({ open: false });
  }

  handleOpen() {
    this.setState({ open: true });
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Button onClick={this.handleOpen} variant="outlined" color="secondary">Delete Event</Button>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
            <Typography variant="title" id="modal-title">
              Delete Calendar Event
            </Typography>
            <form onSubmit={this.handleSubmit}>
              <TextField
                id="inputId"
                label="Event ID"
                className={classes.textField}
                inputRef={this.inputId}
                margin="normal"
              />
              <br />
              <Button color="primary" variant="contained" className={classes.button} type="submit">
                Delete
              </Button>
            </form>
          </div>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    accounts: state.accounts,
    contracts: state.contracts,
    web3: state.web3
  };
}

const connectedForm = connect(mapStateToProps)(DeleteEventForm);

export default withStyles(styles)(connectedForm);
