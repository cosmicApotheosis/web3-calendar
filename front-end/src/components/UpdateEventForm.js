import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
});

class UpdateEventForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };

    // modal handlers
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    // form is uncontrolled component
    this.handleSubmit = this.handleSubmit.bind(this);
    // create references
    this.inputId = React.createRef();
    this.inputTitle = React.createRef();
    this.inputStartDate = React.createRef();
    this.inputEndDate = React.createRef();
    this.inputDesc = React.createRef();
  }

  async componentDidMount() {
    try {
      // local web3 variables
      this.web3 = this.props.web3;
      this.calendarFactoryInstance = this.props.contracts[0];
      this.account = this.props.accounts[0];
    } catch (error) {
      console.log(error);
    }
  }

  async updateCalendarEvent(id, title, start, end, desc) {
    const updatedEvent = await this.calendarFactoryInstance.updateCalendarEvent(id, title, start, end, desc, { from: this.account });
    return updatedEvent;
  }

  // contract date input is in unix time
  convertToUnixTime(time) {
    let date1 = new Date(time);
    return date1.getTime() / 1000;
  }

  async handleSubmit(event) {
    event.preventDefault();

    const id = this.inputId.current.value;
    const title = this.inputTitle.current.value;
    const start = this.convertToUnixTime(this.inputStartDate.current.value);
    const end = this.convertToUnixTime(this.inputEndDate.current.value);
    const desc = this.inputDesc.current.value;

    console.log(`submitted id: ${id}`);
    console.log(`submitted title: ${title}`);
    console.log(`submitted start: ${start}`);
    console.log(`submitted end: ${end}`);
    console.log(`submitted desc: ${desc}`);

    await this.updateCalendarEvent(id, title, start, end, desc);

    // reset form values
    this.inputId.current.value = null;
    this.inputTitle.current.value = null;
    this.inputStartDate.current.value = null;
    this.inputEndDate.current.value = null;
    this.inputDesc.current.value = null;
    // close modal
    this.handleClose();
  }

  handleClose() {
    this.setState({ open: false });
  }

  handleOpen() {
    this.setState({ open: true });
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Button onClick={this.handleOpen} variant="outlined" color="primary">Update Existing Event</Button>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
            <Typography variant="title" id="modal-title">
              Update Calendar Event
            </Typography>
            <form onSubmit={this.handleSubmit}>
              <TextField
                id="inputId"
                label="Event ID"
                className={classes.textField}
                inputRef={this.inputId}
                margin="normal"
              />
              <TextField
                id="inputTitle"
                label="Event Title"
                className={classes.textField}
                inputRef={this.inputTitle}
                margin="normal"
              />
              <br />
              <br />
              <TextField
                id="inputStartDate"
                label="Event Start Date"
                type="datetime-local"
                className={classes.textField}
                inputRef={this.inputStartDate}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <br />
              <br />
              <TextField
                id="inputEndDate"
                label="Event End Date"
                type="datetime-local"
                className={classes.textField}
                inputRef={this.inputEndDate}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                id="inputDesc"
                label="Event Description"
                className={classes.textField}
                inputRef={this.inputDesc}
                margin="normal"
              />
              <br />
              <Button color="primary" variant="contained" className={classes.button} type="submit">
                Update
              </Button>
            </form>
          </div>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    accounts: state.accounts,
    contracts: state.contracts,
    web3: state.web3
  };
}

const connectedForm = connect(mapStateToProps)(UpdateEventForm);

export default withStyles(styles)(connectedForm);
