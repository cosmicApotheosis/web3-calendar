import React, { Children, Component } from 'react';
import { connect } from 'react-redux';

import { initalizeDappState } from '../actions';
//import SimpleStorageContract from '../contracts/SimpleStorage.json';
import CalendarFactory from '../contracts/CalendarFactory.json';


class Web3Load extends Component {

  componentWillMount() {
    // thunk action creator that instantiates web3, contract, and accounts
    this.props.initalizeDappState(CalendarFactory);
  }

  render() {
    console.groupEnd();
    if (this.props.web3 && this.props.accounts && this.props.contracts.length) {
      return Children.only(this.props.children);
    }

    return (
      <div>
        loading dapp
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    accounts: state.accounts,
    contracts: state.contracts,
    //ipfs: state.ipfs,
    web3: state.web3
  };
}

export default connect(mapStateToProps, { initalizeDappState })(Web3Load);
