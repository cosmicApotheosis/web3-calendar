import React, { Component } from 'react';
import { connect } from 'react-redux';
import { calendarCreated } from '../actions';

export default (ChildComponent) => {
  class ComposedComponent extends Component {
    // our component just got rendered
    componentDidMount() {
      this.shouldNavigateAway();
    }

    // our component just got updated
    componentDidUpdate() {
      this.shouldNavigateAway();
    }

    async shouldNavigateAway() {
      const hasCal = await this.props.contracts[0].hasCalendar(this.props.accounts[0]);

      if (hasCal) {
        this.props.calendarCreated();
        console.log('I NEED TO LEAVE!!!!');
        this.props.history.push(`/${this.props.accounts[0]}`);
      }
    }

    render() {
      return (
        <ChildComponent {...this.props} />
      );
    }
  }

  function mapStateToProps(state) {
    return { 
        accounts: state.accounts,
        contracts: state.contracts,
    };
  }

  return connect(mapStateToProps, { calendarCreated })(ComposedComponent);
};
