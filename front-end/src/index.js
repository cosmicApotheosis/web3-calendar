import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import Web3Load from './components/Web3Load';
import CalendarShow from './components/CalendarShow';
import CalendarCreate from './components/CalendarCreate';

import store from './store';

ReactDOM.render(
  <Provider store={store}>
    <Web3Load>
      <BrowserRouter>
        <div>
          <Switch>
            <Route path="/:account" component={CalendarShow} />
            <Route path="/" component={CalendarCreate} />
          </Switch>
        </div>
      </BrowserRouter>
    </Web3Load>
  </Provider>,
  document.getElementById('root')
);
