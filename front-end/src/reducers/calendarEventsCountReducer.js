import { GET_CALENDAR_EVENTS_COUNT } from '../actions/types';

const initialState = 0;

const calendarEventsCountReducer = (state = initialState, action) => {
  if (action.type === GET_CALENDAR_EVENTS_COUNT) {
    console.log('calendarEventsCountReducer action: ', action);
    const newState = action.payload;
    console.log('state', state);
    console.log('newState', newState);
    return newState;
  }
  return state;
}

export default calendarEventsCountReducer;
