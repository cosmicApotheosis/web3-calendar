import { GET_CALENDAR_EVENTS } from '../actions/types';

const initialState = [];

const calendarEventsReducer = (state = initialState, action) => {
  if (action.type === GET_CALENDAR_EVENTS) {
    console.log('calendarEventsReducer action: ', action);
    const newState = action.payload;
    console.log('state', state);
    console.log('newState', newState);
    return newState;
  }
  return state;
}

export default calendarEventsReducer;
