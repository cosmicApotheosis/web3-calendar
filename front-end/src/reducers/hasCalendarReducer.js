import { CALENDAR_CREATED } from '../actions/types';

const initialState = null;

const hasCalendarReducer = (state = initialState, action) => {
  if (action.type === CALENDAR_CREATED) {
    console.log('hasCalendarReducer action: ', action);
    return action.payload;
  }
  return state;
}

export default hasCalendarReducer;
