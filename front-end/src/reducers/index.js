import { combineReducers } from 'redux';
import web3Reducer from './web3Reducer';
import accountsReducer from './accountsReducer';
import contractsReducer from './contractsReducer';
import hasCalendarReducer from './hasCalendarReducer';
import calendarEventsCountReducer from './calendarEventsCountReducer';
import calendarEventsReducer from './calendarEventsReducer';
//import ipfsReducer from './ipfsReducer';

const rootReducer = combineReducers({
  accounts: accountsReducer,
  calendarEvents: calendarEventsReducer,
  calendarEventsCount: calendarEventsCountReducer,
  contracts: contractsReducer,
  hasCalendar: hasCalendarReducer,
  //ipfs: ipfsReducer,
  web3: web3Reducer
});

export default rootReducer;
