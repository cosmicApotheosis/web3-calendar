import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import ReduxPromise from 'redux-promise';

import rootReducer from './reducers';

const store = createStore(rootReducer, applyMiddleware(ReduxPromise, reduxThunk));

export default store;
