var CalendarFactory = artifacts.require("CalendarFactory");
var Calendar = artifacts.require("Calendar");

assertRevert = async promise => {
  try {
    await promise;
    assert.fail('Expected revert not received');
  } catch (error) {
    const revertFound = error.message.search('revert') >= 0;
    assert(revertFound, `Expected "revert", got ${error} instead`);
  }
};

assertDestroyedContract = async promise => {
  try {
    await promise;
    assert.fail('Contract still exists');
  } catch (error) {
    const destroyedContractFound = error.message.search('not a contract address') >= 0;
    assert(destroyedContractFound, `Expected 'not a contract address', got ${error} instead`);
  }
};

let CalendarInstanceAddr;
let CalendarInstance;

contract('CalendarFactory', accounts => {

  it("should assert true", done => {
    let calendar_factory = CalendarFactory.deployed();
    assert.isTrue(true);
    done();
  });
  // this test is to ensure the factory contract begins without and calendars stored in the list.
  // the factory contract has no constructor
  it("getCalendarCount() returns 0 to begin with", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(instance => {
      calendar_factory = instance;
      return calendar_factory.getCalendarCount();
    }).then((result) => {
      assert.equal(result.toNumber(), 0);
      //console.log(result.toNumber());
      //console.log(result);
    })
  })
  // This checks that a new calendar is indeed added to the array that tracks users calendars.
  // the event is necessary for the front-end UI to update.
  it("createNewCalendar() adds new Calendar contract instance to calendars array and emits an event", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(instance => {
      calendar_factory = instance;
      return calendar_factory.createNewCalendar(accounts[0]);
    }).then((result) => {
      assert.equal(result.logs[0].event, 'NewCalendarAdded');
    }).then(() => {
      return calendar_factory.getCalendarCount();
    }).then((result) => {
      //console.log((result));
      assert.equal(result.toNumber(), 1);
      //console.log(result.toNumber());
      //console.log(result);
      return calendar_factory.getCalendarAddress(accounts[0], { from: accounts[0] });
    }).then((result) => {
      // save calendar instance address, create truffle calendar instance
      CalendarInstanceAddr = result;
      CalendarInstance = Calendar.at(CalendarInstanceAddr);
      // just check that the public getter function for the owner works
      return CalendarInstance.owner();
    }).then(result => {
      //console.log(result);
    })
  })
  // Ensures that the same address isn't added twice to the mapping. 
  it("hasCalendar detects if someone trys to make a new calendar if they already got one", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(instance => {
      calendar_factory = instance;
      return calendar_factory.hasCalendar(accounts[0]);
    }).then((result) => {
      assert.isOk(result);
    })
  })
  // The app wouldn't be very useful if anyone could just delete someone else's calendar. 
  it("a user cant delete someone elses calendar", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(async instance => {
      calendar_factory = instance;
      // should just revert at this point
      await assertRevert(calendar_factory.deleteCalendar(accounts[0], { from: accounts[1] }));
    })
  })
  // In addition to removing the calendar from the factory list, we check to ensure that the calendar contract
  // is indeed destroyed. This is a test of the Destructable OZ contract implement
  it("a user can delete their calendar", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(instance => {
      calendar_factory = instance;
      return calendar_factory.deleteCalendar(accounts[0], { from: accounts[0] });
    }).then((result) => {
      //console.log(result);
      assert.equal(result.logs[0].event, 'CalendarDeleted');
    }).then(() => {
      return calendar_factory.getCalendarCount();
    }).then(async (result) => {
      assert.equal(result.toNumber(), 0);
      // this checks that the calendar instance does not exist
      await assertDestroyedContract(CalendarInstance.owner());
    })
  })
  

  // starts with 1 event since a dummy event is added to address the deletion issue 
  // mentioned in the contract. This is a hack that needs to be addressed
  it("getCalendarEventsCount() returns 1 to begin with for the child contract", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(instance => {
      calendar_factory = instance;
      return calendar_factory.createNewCalendar(accounts[0]);
    }).then(() => {
      return calendar_factory.getCalendarEventsCount();
    }).then((result) => {
      assert.equal(result.toNumber(), 0 + 1);
    })
  })
  // The event is emitted in the user's calendar contract instance, not the factory instance.
  // The UI needs to interact with both contracts.
  it("user can create a new calendar event in his contract using the calendar factory function", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(instance => {
      calendar_factory = instance;
      return calendar_factory.createNewCalendarEvent('sample event', 600, 800, 'menial bs', { from: accounts[0] });
    }).then(() => {
      return calendar_factory.getCalendarEventsCount();
    }).then((result) => {
      assert.equal(result.toNumber(), 1 + 1);
    })
  })
  // To ensure behavior is consistent. 
  it("user can create a second calendar event in his contract using the calendar factory function", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(instance => {
      calendar_factory = instance;
      return calendar_factory.createNewCalendarEvent('second event', 6, 8001, 'menial bs', { from: accounts[0] });
    }).then(() => {
      return calendar_factory.getCalendarEventsCount();
    }).then((result) => {
      assert.equal(result.toNumber(), 2 + 1);
    })
  })
  // This is to check that an array with all event ids is accessible to the UI.
  // This array of IDs is looped over with the getCalendarEvent function..
  it("user can get all the events in his contract using the calendar factory function", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(instance => {
      calendar_factory = instance;
      return calendar_factory.getAllCalendarEvents.call();
    }).then((result) => {
      assert.equal(result.map(index => index.toNumber()).length, 2 + 1);
    })
  })
  // These details are stored in the UI redux state and then given as props to the calendar component.
  it("user can get details of a given event in his contract using the calendar factory function", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(instance => {
      calendar_factory = instance;
      return calendar_factory.getCalendarEvent(1000);
    }).then((result) => {
      assert.equal(result[0].toNumber(), 1000);
      assert.equal(result[1], 'sample event');
      assert.equal(result[2].toNumber(), 600);
      assert.equal(result[3].toNumber(), 800);
      assert.equal(result[4], 'menial bs');
    })
  })
  // This prevents the events array in the UI from displaying events that dont exist in the calendar.
  it("user can't get details of a given event that doesn't exist in his contract using the calendar factory function", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(async instance => {
      calendar_factory = instance;
      // revert
      await assertRevert(calendar_factory.getCalendarEvent(1010));
    })
  })
  // A user needs to be able to update an event in his calendar.
  it("user can update a calendar event in his contract using the calendar factory function", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(instance => {
      calendar_factory = instance;
      calendar_factory.updateCalendarEvent(1000, 'updated sample event', 602, 840, 'more menial bs');
    }).then((result) => {
      //console.log(result);
    }).then(() => {
      return calendar_factory.getCalendarEvent(1000);
    }).then((result) => {
      assert.equal(result[0].toNumber(), 1000);
      assert.equal(result[1], 'updated sample event');
      assert.equal(result[2].toNumber(), 602);
      assert.equal(result[3].toNumber(), 840);
      assert.equal(result[4], 'more menial bs');
    })
  })
  // ensures the tx reverts
  it("user can't update an event that doesn't exist in his contract using the calendar factory function", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(async instance => {
      calendar_factory = instance;
      // revert
      await assertRevert(calendar_factory.updateCalendarEvent(1010, 'updated sample event', 602, 840, 'more menial bs'));
    })
  })
  // the list needs to maintain integrity after delete. 
  it("user can get delete an event in his contract using the calendar factory function", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(instance => {
      calendar_factory = instance;
      return calendar_factory.deleteCalendarEvent(1000);
    }).then((result) => {
      //console.log(result);
    }).then(() => {
      return calendar_factory.getCalendarEventsCount();
    }).then((result) => {
      assert.equal(result.toNumber(), 1 + 1);
    })
  })
  // make sure it reverts. 
  it("user can't delete an event that doesn't exist in his contract using the calendar factory function", () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(async instance => {
      calendar_factory = instance;
      // revert
      await assertRevert(calendar_factory.deleteCalendarEvent(1010));
    })
  })
  // In practice, the admin would enact this using Remix. 
  it("admin can enact emergency stop to prevent users from creating calendars", async () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(async instance => {
      calendar_factory = instance;
      return calendar_factory.pause();
    }).then(async () => {
      // revert
      await assertRevert(calendar_factory.createNewCalendar(accounts[0]));
    }).then(() => {
      return calendar_factory.unpause();
    })
  })
  // if a non admin could freeze the contract that wouldn't be very secure
  it("NON admin can NOT enact emergency stop", async () => {
    let calendar_factory;
    return CalendarFactory.deployed().then(async instance => {
      calendar_factory = instance;
      await assertRevert(calendar_factory.pause({ from: accounts[1] }));
    })
  })
});
