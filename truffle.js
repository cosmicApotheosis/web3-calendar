// var HDWalletProvider = require("truffle-hdwallet-provider");
// var mnemonic = "fit road defense sorry stumble siren start wide enforce hood chief chalk";
// 0x6e2f3c1168b1BBFc9731c4ce6812C341aA92F9e8

module.exports = {
  migrations_directory: "./migrations",
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*" // Match any network id
    },
    // rinkeby: {
    //   provider: function() {
    //     return new HDWalletProvider(mnemonic, "https://rinkeby.infura.io/S8N9OYoj6VsnKbEYDzwh")
    //   },
    //   network_id: 4
    // }
  },
  solc: {
    optimizer: {
      enabled: true,
      runs: 500
    }
  }
};
